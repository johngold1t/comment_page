<?php
$connect = new PDO('sqlite:data.db');

function view()
{
    global $connect;

    $query = $connect->query('SELECT * from comments');

    return $query->fetchAll(PDO::FETCH_ASSOC);
}

