<?php
require_once 'functions.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        body {
            background: #eee
        }

        .date {
            font-size: 11px
        }

        .comment-text {
            font-size: 12px
        }

        .fs-12 {
            font-size: 12px
        }

        .shadow-none {
            box-shadow: none
        }

        .name {
            color: #007bff
        }

        .cursor:hover {
            color: blue
        }

        .cursor {
            cursor: pointer
        }

        .textarea {
            resize: none
        }
    </style>
    <title>Отзывы</title>
</head>
<body>
<div class="container mt-5">
    <div class="d-flex justify-content-center row">
        <div class="col-md-8">
            <div class="d-flex flex-column comment-section">
                <?php foreach(view() as $comment):?>
                <div class="bg-white p-2">
                    <div class="d-flex flex-row user-info"><img class="rounded-circle"
                                                                src="https://i.imgur.com/RpzrMR2.jpg" width="40">
                        <div class="d-flex flex-column justify-content-start ml-2">
                            <span class="d-block font-weight-bold name"><?php echo $comment['name']?></span>
                            <span lass="date text-black-50"><?php echo $comment['date']?></span></div>
                    </div>
                    <div class="mt-2">
                        <p class="comment-text">
                            <?php echo $comment['desc']?>
                        </p>
                    </div>
                </div>
                <?php endforeach;?>
<hr/>
                <div class="bg-light p-2">
                    <form action="/add.php" method="post">
                    <div class="d-flex flex-row align-items-start">
                        <img class="rounded-circle"  src="https://i.imgur.com/RpzrMR2.jpg" width="40">
                        <input type="name" value="">
                        <textarea name="desc" class="form-control ml-1 shadow-none textarea"></textarea>
                    </div>
                    <div class="mt-2 text-right">
                        <button class="btn btn-primary btn-sm shadow-none" type="button">Post comment</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>